#!/bin/bash
SNPGENES_DIR=$1
OUTPUT_DIR=$2

mkdir -p ${OUTPUT_DIR}/gtex_bed_files

for EQTL_F in ${SNPGENES_DIR}/*snpgenes; do
    FNAME=`basename ${EQTL_F}`
    TISS=${FNAME%_Analysis.snpgenes}
    tail -n +2 ${EQTL_F} | awk -F$'\t'  'BEGIN{OFS=FS} {printf "chr%s\t%d\t%d\t%s\n", $14, $15, $15+1, $27}' > ${OUTPUT_DIR}/gtex_bed_files/${TISS}_eQTLs.bed
done
