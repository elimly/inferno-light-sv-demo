#!/bin/bash
ROADMAP_DIR=$1
OUTPUT_DIR=$2

mkdir -p ${OUTPUT_DIR}/roadmap_enh_bed_files

for RM_FILE in ${ROADMAP_DIR}/*_15_coreMarks_mnemonics.bed; do
	FNAME=`basename ${RM_FILE} _15_coreMarks_mnemonics.bed`
	grep "Enh" $RM_FILE > ${OUTPUT_DIR}/roadmap_enh_bed_files/${FNAME}_enhancers.bed
done 

