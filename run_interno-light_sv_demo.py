#!/usr/bin/python3

import os, warnings, argparse, sys
warnings.filterwarnings('ignore')
os.environ['PYSPARK_PYTHON'] = '/usr/bin/python3'

from pyspark.sql import SparkSession

import spark_df_functions as sdf_fxn
import interval_annotation as annot



##########################################################
# ** TEMPORARILY hardcoded Global Variables **

## Pipeline configuration - user should NOT change these!
NUM_TISS_CAT = 32
ANNOT_VAR_FILE = "config/annotation_info/inferno-light_annotation_config.tsv"
SHELL_DIR =  "src/spark_output_shell_scripts"


## additional configuration -- these could change
OUT_DIR = "demo_output_files"
QUERY_COLS = ['q_chr', 'q_start', 'q_end']
FT_COLS = ['ft_chr', 'ft_start', 'ft_end', 'ft_name', 'ft_file', 'hit_str']
OVRLP_COLS = ['ovrlp_chr', 'ovrlp_start', 'ovrlp_end']
##########################################################

## TODO: NEED to dynamically set ws_home & absolute paths




def interval_annotation_inferno_light(query_sdf, annot_type, annot_dict, query_cols, ft_cols, ovrlp_cols, ws_home, out_dir, shell_dir, spark_sess):
	"""
	Processes the interval annotation overlap for the specified Annotation type (annot_type)
	
	:param query_sdf: (Spark DF) input query interval DF
			>> MUST contain chr, start & end columns -- name of columns is flexible
	:param annot_type: (str) annotation type - lower case
	:param annot_dict: (dict) annotation variable configuration
			>> must have the following fields:
				'annot_abr' -- (str) abbreviation for this annotation type
				'gadb_giggle_str' -- (str) GADB filter query string
						*NOTE: currently set to tmp parsed GIGGLE file relative path
				'annot_info_file_path' -- (str) relative path to the annot info file
				'annot_info_join_col' -- (str) column name in annot info DF to join on
				'tissue_info' -- (bool) True= tissue info, False= no tissue info
				
				'rename_ft_dict' (dict) map to rename generic 'ft' columns --> annot specific name
				'rename_ovrlp_dict' (dict) map generic column name --> annot specific column name
				
	:param query_cols: (list) current query column names
	:param ft_cols: (list) current annotation feature column names
	:param ovrlp_cols: (list) current overlap column names
	:param ws_home: (str) current working directory path
	:param out_dir: (str) output directory ABSOLUTE path
	:param shell_dir: (str) shell script directory ABSOLUTE
	:param spark_sess: current Spark Session
	:return: dictionary of PySpark DataFrames
			returns for all annot types:
				'overlap' = query-annotation overlap DF - with added annot info & overlap data
				'int_summ' = interval summary DF --> for combined interval summary DF
				
			if tissue specific annot, also return:
				'tc_cnt' = tissue category count DF --> for combined Tiss Cat count DF
				'tcm' = tissue category matrix DF --> for multiway overlaps & combined Tiss Cat count DF
	"""
	annot_abr = annot_dict[annot_type]['annot_abr']
	annot_ft_dict = annot_dict[annot_type]['rename_ft_dict']
	giggle_str = os.path.join(ws_home, annot_dict[annot_type]['gadb_giggle_str'])
	
	## hardcoded for now - #TODO: make dynamic
	join_col_ovrlp = 'ft_file'
	col_tissue = 'tissue_name'
	col_category = 'tiss_cat_name'
	col_cat_id = 'tiss_cat_id'
	num_tiss_cat = 32
	
	## return a dict of PySpark DFs
	return_sdf_dict = {}
	
	##################################
	#### 1) run GIGGLE search
	ovrlp_sdf = annot.run_GIGGLE_search(query_sdf, annot_type, giggle_str, spark_sess)
	
	
	##################################
	#### 2) OPTIONAL: add annotation specific information / metadata
	if annot_dict[annot_type]['annot_info_file_path'] is not None:
		annot_info_file = os.path.join(ws_home, annot_dict[annot_type]['annot_info_file_path'])
		join_col_tiss = annot_dict[annot_type]['annot_info_join_col']
	
		ovrlp_sdf = annot.add_annot_info_from_file(ovrlp_sdf, annot_info_file,
															 join_col_ovrlp, join_col_tiss,
															 spark_sess, sep=',')
	
	##################################
	#### 3) extract overlap info
	ovrlp_sdf = annot.extract_overlap_coord(ovrlp_sdf, query_cols, ft_cols, ovrlp_cols)
	
	## OPTIONAL: extract length & overlap proportion of query & feature
	cols_start_end = annot.get_start_end_cols(query_cols, ft_cols, ovrlp_cols)
	ovrlp_sdf = annot.extract_overlap_length_data(ovrlp_sdf, *cols_start_end)
	
	#TODO: reorder ovrlp_sdf columns
	
	
	##################################
	#### 4) generate interval summary
	if annot_dict[annot_type]['tissue_info']:
		interval_summ_sdf = annot.interval_summary_tissue(ovrlp_sdf, annot_type, annot_abr, query_cols, ft_cols, ovrlp_cols)
	else:
		interval_summ_sdf = annot.interval_summary(ovrlp_sdf, annot_type, annot_abr, query_cols, ft_cols, ovrlp_cols)
	
	## convert ArrayType->StringType && rename interval summary columns && add to return_dict
	interval_summ_sdf = annot.rename_annotation_columns(interval_summ_sdf, annot_ft_dict)
	interval_summ_sdf = sdf_fxn.convert_ArrayType_columns(interval_summ_sdf)
	return_sdf_dict['int_summ'] = interval_summ_sdf
	
	## write interval_summ output file
	write_int_summ_return = annot.write_interval_summary_output_file(interval_summ_sdf, annot_type, query_cols, out_dir, shell_dir)
	print("interval summary write output return: ", write_int_summ_return)
	
	
	##################################
	#### 5) Tissue - Specific processing
	## Tissue Category counts
	if annot_dict[annot_type]['tissue_info']:
		#### Tissue Category counts
		tiss_cat_cnt_sdf = annot.get_tissue_cat_total_counts(ovrlp_sdf, annot_type, annot_abr, ft_cols, ovrlp_cols, num_tissue_cat=num_tiss_cat, spark_session=spark_sess)
		
		## rename interval summary columns && add to return_dict
		tiss_cat_cnt_sdf = annot.rename_annotation_columns(tiss_cat_cnt_sdf, annot_ft_dict)
		return_sdf_dict['tc_cnt'] = tiss_cat_cnt_sdf
		
		
		#### Tissue Category Matrix: groupBy query + overlap  --> use for multiway overlap
		grpby_cols = query_cols + ovrlp_cols
		tcm_sdf = annot.generate_tiss_category_matrix(ovrlp_sdf, query_cols, grpby_cols, col_tissue, col_category, col_cat_id, num_tiss_cat)
		
		## convert ArrayType->StringType && add to return_dict
		tcm_sdf = sdf_fxn.convert_ArrayType_columns(tcm_sdf) ## convert lists / sets to string representation
		return_sdf_dict['tcm'] = tcm_sdf
	
	
	## at end, rename ovrlp_sdf columns && add to return_dict
	ovrlp_sdf = annot.rename_annotation_columns(ovrlp_sdf, annot_dict[annot_type]['rename_ovrlp_dict'])
	#TODO write ovrlp_sdf output file?
	return_sdf_dict['overlap'] = ovrlp_sdf
	
	return return_sdf_dict







def driver_inferno_light_sv_demo(query_sdf, annot_list, annot_dict, query_cols, ft_cols, ovrlp_cols, num_tiss_cat, out_dir, shell_dir, spark_sess):
	"""
	1) Calls 'interval_annotation_inferno_light' for each annotation type in list
	2) join individual interval_summary DFs ==> combined interval_summary table (for INFERNO light)
	3) identifies FANTOM5-Roadmap two-way overlaps - by Tissue Category
	4) join individual Tissue Category count DFs ==> combined tissue_category_count table
	
	:param query_sdf: (Spark DF) input query interval DF
			>> MUST contain chr, start & end columns -- name of columns is flexible
	:param annot_list: (list) annotation types to run
	:param annot_dict: (dict) annotation variable configuration
			>> must have the following fields:
				'annot_abr' -- (str) abbreviation for this annotation type
				'gadb_giggle_str' -- (str) GADB filter query string
						*NOTE: currently set to tmp parsed GIGGLE file relative path
				'annot_info_file_path' -- (str) relative path to the annot info file
				'annot_info_join_col' -- (str) column name in annot info DF to join on
				'tissue_info' -- (bool) True= tissue info, False= no tissue info
				
				'rename_ft_dict' (dict) map to rename generic 'ft' columns --> annot specific name
				'rename_ovrlp_dict' (dict) map generic column name --> annot specific column name
				
	:param query_cols: (list) current query column names
	:param ft_cols: (list) current annotation feature column names
	:param ovrlp_cols: (list) current overlap column names
	:param num_tiss_cat: (int) number of INFERNO Tissue Categories
				(currently = 32 but may change in the future?)
	:param out_dir: (str) output directory ABSOLUTE path
	:param shell_dir: (str) shell script directory ABSOLUTE
	:param spark_sess: current Spark Session
	:return: nested dictionary of PySpark DataFrames
	"""
	## setup
	ws_home = shell_dir.split('src')[0] ## assumes ws_home/src
	col_cat_id = 'tiss_cat_id'
	f5_abr = annot_dict['fantom5']['annot_abr']
	rm_abr = annot_dict['roadmap']['annot_abr']
	   
	##################################
	#### 1) run interval_annotation_inferno_light on each annotation type
	annot_sdf_dict = {a:interval_annotation_inferno_light(query_sdf, a, annot_dict, query_cols,
													  ft_cols, ovrlp_cols, ws_home,
													  out_dir, shell_dir, spark_sess)
					  for a in annot_list}
	
	##################################
	#### 2) extract PySpark DataFrames from annotation result dictionary
	f5_summ_sdf = annot_sdf_dict['fantom5']['int_summ']
	f5_ovrlp_sdf = annot_sdf_dict['fantom5']['overlap']
	f5_tc_cnt_sdf = annot_sdf_dict['fantom5']['tc_cnt']
	f5_tcm_sdf = annot_sdf_dict['fantom5']['tcm']

	rm_summ_sdf = annot_sdf_dict['roadmap']['int_summ']
	rm_ovrlp_sdf = annot_sdf_dict['roadmap']['overlap']
	rm_tc_cnt_sdf = annot_sdf_dict['roadmap']['tc_cnt']
	rm_tcm_sdf = annot_sdf_dict['roadmap']['tcm']
	
	if 'homer' in annot_list:
		homer_summ_sdf = annot_sdf_dict['homer']['int_summ']
		homer_ovrlp_sdf = annot_sdf_dict['homer']['overlap']
		
		interval_summ_sdf_list = [f5_summ_sdf, rm_summ_sdf, homer_summ_sdf]
	else:
		interval_summ_sdf_list = [f5_summ_sdf, rm_summ_sdf]
	
	##################################
	#### 3) join interval summary DFs (ArrayType->StringType converted interval summary DFs)
	combined_interval_summ_sdf = annot.join_interval_summary(query_sdf, interval_summ_sdf_list, query_cols)
	# print(sdf_fxn.print_sdf(combined_interval_summ_sdf))

	##################################
	#### 4) count multiway overlaps by Tissue Category
	## FANTOM5 - Roadmap chromHMM 2way overlap - by Tissue Category
	twoway_f5_rm = annot.fantom5_roadmap_overlap(f5_tcm_sdf, rm_tcm_sdf, query_cols, ovrlp_cols)

	## convert ArrayType->StringType (needed for counting # 2way overlaps & writing output file)
	twoway_f5_rm = sdf_fxn.convert_ArrayType_columns(twoway_f5_rm)
	
	## count # of FANTOM5-Roadmap 2way overlaps per Tissue Category  (args declared above)
	f5_rm_2way_cnt_sdf = annot.count_twoway_ovrlp_by_tiss_cat(twoway_f5_rm, f5_abr, rm_abr, col_cat_id, num_tiss_cat, spark_sess)

	##################################
	#### 5) join Tissue Category count DFs
	## list of Tissue Category count DFs to join
	tc_sdf_list = [f5_tc_cnt_sdf, rm_tc_cnt_sdf, f5_rm_2way_cnt_sdf]

	## join Tissue Category count DFs
	combined_tc_cnt = annot.join_tissue_category_counts(tc_sdf_list, col_cat_id)
	# print(sdf_fxn.print_sdf(combined_tc_cnt))
	
	##################################
	#### 6) write output files
	write_int_summ_return = annot.write_interval_summary_output_file(combined_interval_summ_sdf, "ANNOT-COMBINED", query_cols, out_dir, shell_dir)
	print("interval summary write output return: ", write_int_summ_return)
	
	
	#TODO: write combined tiss cat output file


	##################################
	#### 7) add combined DFs to annot_sdf_dict --> return annot_sdf_dict
	join_sdf_dict = {'int_summ':combined_interval_summ_sdf , 'tc_cnt':combined_tc_cnt}
	annot_sdf_dict['combined'] = join_sdf_dict

	return annot_sdf_dict
	


def run_inferno_light_sv_demo(query_file, annot_var_file, annot_list, query_cols, ft_cols, ovrlp_cols, num_tiss_cat, out_dir, shell_dir, spark_sess):
	"""
	Wrapper fxn that calls 'driver_inferno_light_sv_demo' with the current configuration settings
	
	:param query_file: (str) ABSOLUTE path to the input query interval file
	:param annot_var_file: (str) relative path to the annotation configuration file
	:param annot_list: (list) annotation types to run
	:param query_cols: (list) current query column names
	:param ft_cols: (list) current annotation feature column names
	:param ovrlp_cols: (list) current overlap column names
	:param num_tiss_cat: (int) number of INFERNO Tissue Categories
				(currently = 32 but may change in the future?)
	:param out_dir: (str) relative path to the output directory
	:param shell_dir: (str) relative path to the shell script directory
	:param spark_sess: current Spark Session
	:return: (combined interval_summary DF, combined tiss_category_count DF)
	"""
	
	## setup absolute paths
	ws_home = os.getcwd()
	while not os.path.isdir(os.path.join(ws_home, 'src')): #TODO: remove this?
		ws_home = ws_home.rpartition('/')[0]
	sys.path.insert(0, ws_home)
	
	annot_var_file = os.path.join(ws_home, annot_var_file)
	out_dir = os.path.join(ws_home, out_dir)
	shell_dir = os.path.join(ws_home, shell_dir)
	query_file = os.path.join(ws_home, query_file)
	

	## load query file
	query_sdf = sdf_fxn.load_name_columns(query_file, '\t', query_cols, spark_sess)
	# print(sdf_fxn.print_sdf(query_sdf))

	## generate annotation information dictionary:
	annot_config_dict = annot.generate_annot_config_dict(annot_var_file)
	# print(annot_config_dict.keys())
	
	##################################
	#### run inferno-light demo
	if len(annot_list) == 1: ## run single annotation
		sdf_dict = interval_annotation_inferno_light(query_sdf, annot_list[0], annot_config_dict, query_cols, ft_cols, ovrlp_cols, ws_home, out_dir, shell_dir, spark_sess)
		return sdf_dict

	else: ## run full demo
		result_sdf_dict = driver_inferno_light_sv_demo(query_sdf, annot_list, annot_config_dict, query_cols, ft_cols, ovrlp_cols, num_tiss_cat, out_dir, shell_dir, spark_sess)

		combined_int_summ_sdf = result_sdf_dict['combined']['int_summ']
		combined_tc_cnt_sdf = result_sdf_dict['combined']['tc_cnt']
		# print(sdf_fxn.print_sdf(combined_int_summ_sdf))

		return (combined_int_summ_sdf, combined_tc_cnt_sdf)
	


## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started wrapper script. Running in all-spark-notebook Docker container\n")
	
	parser = argparse.ArgumentParser()
	## REQUIRED ARGS
	parser.add_argument("interval_file", type=valid_file, action='store', help="path for the query interval input file")
	parser.add_argument("-a", "--annot", dest="annot_list", nargs='+', type=str, help="enter annotation names - SPACE separated")
	
	## optional - for easier testing
	parser.add_argument("--demo", dest="bool_demo", action='store_true', default=False, help="run default demo? includes FANTOM5, Roadmap & HOMER")
	
	## optional configuration
	parser.add_argument("--out_dir", dest="out_dir", type=str, action='store', default=OUT_DIR, help="relative path to the desired output directory")
	parser.add_argument("--num_cores", dest="num_cores", type=int, action='store', default=16, help="# of cores -- used for Spark partitioning")
	
	
	
	#### Parse Args
	pargs = parser.parse_args()
	interval_file = pargs.interval_file
	out_dir = pargs.out_dir
	defaultParallelism = pargs.num_cores
	
	if pargs.bool_demo:
		annot_list = ['fantom5', 'roadmap', 'homer']
	elif pargs.annot_list is not None:
		annot_list = pargs.annot_list
		## if running >1 annotation: MUST include at least FANTOM5 & Roadmap - HOMER is optional
		if len(annot_list) > 1:
			if 'fantom5' not in annot_list: annot_list.append('fantom5')
			if 'roadmap' not in annot_list: annot_list.append('roadmap')
	else:
		annot_list = ['fantom5', 'roadmap', 'homer']
	print(annot_list)
		
		
	
	#### Spark Session setup
	spark_session_name = "inferno-light_sv-demo"
	# spark_sess = SparkSession.builder.appName(spark_session_name).getOrCreate()
	
	spark = SparkSession.builder\
							.appName(spark_session_name)\
							.config("spark.master", "local[*]")\
                            .config("spark.default.parallelism", defaultParallelism )\
							.getOrCreate()
	spark.sparkContext.setLogLevel("WARN")
	sc = spark.sparkContext
	print("\n\n")


	run_inferno_light_sv_demo(interval_file, ANNOT_VAR_FILE, annot_list, QUERY_COLS, FT_COLS, OVRLP_COLS, NUM_TISS_CAT, out_dir, SHELL_DIR, spark)



