# INFERNO-light SV demo

Disclaimers:
- NOTE: requires Python 3.x  
- this has been tested using the 'all-spark-notebook' Docker container & on the wanglab tf.aws instance  
	- additional testing is necessary... stay tuned! 




### run_interno-light_sv_demo.py can be run in 2 ways:
1. full demo with FANTOM5, Roadmap & HOMER 
	- this includes:
		- combined interval summary
		- FANTOM5-Roadmap two-way overlaps
		- combined Tissue Category counts  
		
2. single annotation type  




## run_inferno-light_sv_demo.py example usage  

### To run the full demo:  
```bash
	python3 run_interno-light_sv_demo.py tmp_input_files/query/parliament_pass_dels_collapsed.bed --demo
```  

OR  

```bash
	python3 run_interno-light_sv_demo.py tmp_input_files/query/parliament_pass_dels_collapsed.bed -a fantom5 roadmap homer
```  

#   
* * *  



## To run a single annotation type:

##### Run FANTOM5 interval annotation processing:  

```bash
	python3 run_interno-light_sv_demo.py tmp_input_files/query/parliament_pass_dels_collapsed.bed -a fantom5
```  
  

##### Run Roadmap interval annotation processing:  

```bash
	python3 run_interno-light_sv_demo.py tmp_input_files/query/parliament_pass_dels_collapsed.bed -a roadmap
```  
  


##### Run HOMER interval annotation processing:  

```bash
	python3 run_interno-light_sv_demo.py tmp_input_files/query/parliament_pass_dels_collapsed.bed -a homer
```  
  



#   
* * *  


### Use the --num_cores option to specify the PySpark parallelism / # of partitions (default = 16)  

```bash
	python3 run_interno-light_sv_demo.py tmp_input_files/query/parliament_pass_dels_collapsed.bed --demo --num_cores 48
```  


