#!/usr/bin/python3

import os, warnings
warnings.filterwarnings('ignore')
os.environ['PYSPARK_PYTHON'] = '/usr/bin/python3'

import pandas as pd
import pyspark.sql.functions as F

import spark_df_functions as sdf_fxn ## Spark DF functions custom python module


def print_sdf(sdf, nrow=5):
	print(sdf.limit(nrow).toPandas().head(nrow))
	
##################################################################################################
## Step 1: run GIGGLE search
##################################################################################################

def run_GIGGLE_search(query_sdf, annot_type, gadb_giggle_str, spark_session):
	print("\n\nrunning \'GIGGLE search\' demo -- annotation = ", annot_type)
	#TODO: implement!
	# gig_result_sdf = gig.search(query_sdf)
	
	#TEMP - remove!!
	gig_pd_df = pd.read_table(gadb_giggle_str, sep='\t', header=0)
	gig_result_sdf = spark_session.createDataFrame(gig_pd_df)
	
	return gig_result_sdf





##################################################################################################
## Step 2: process GIGGLE results sDF & extract overlap information
##################################################################################################

#### 2.a) extract overlap data:
def extract_overlap_coord(ovrlp_sdf, query_cols, ft_cols, ovrlp_cols):
	print("running extract_overlap_coord fxn")

	#TODO: decide what to do with HOMER duplicates!!!
	## drop duplicate from ovrlp_sdf (if necessary)
	ovrlp_sdf = ovrlp_sdf.dropDuplicates()

	## extract overlap info:
	if 'chr' in ovrlp_cols[0]:
		ovrlp_start = ovrlp_cols[1]
		ovrlp_end = ovrlp_cols[2]
		ovrlp_sdf = ovrlp_sdf.withColumn(ovrlp_cols[0], F.col(query_cols[0]))
		
	else:
		ovrlp_start = ovrlp_cols[0]
		ovrlp_end = ovrlp_cols[1]
	
	ovrlp_sdf = ovrlp_sdf.withColumn(ovrlp_start, F.greatest(F.col(query_cols[1]), F.col(ft_cols[1])))\
						.withColumn(ovrlp_end, F.least(F.col(query_cols[2]), F.col(ft_cols[2])))
	
	return ovrlp_sdf


#### 2.b) extract overlap length & proportions:
def extract_overlap_length_data(ovrlp_sdf, q_start, q_end, ft_start, ft_end, ovrlp_start, ovrlp_end):
	print("running extract_overlap_length_data fxn")
	
	q_len = q_start.split('_')[0]+"_len"
	ft_len = ft_start.split('_')[0]+"_len"
	ovrlp_len = ovrlp_start.split('_')[0]+"_len"
	
	ovrlp_sdf = ovrlp_sdf.withColumn(q_len, ovrlp_sdf[q_end]+1 - ovrlp_sdf[q_start])\
						.withColumn(ft_len, ovrlp_sdf[ft_end]+1 - ovrlp_sdf[ft_start])\
						.withColumn(ovrlp_len, ovrlp_sdf[ovrlp_end]+1 - ovrlp_sdf[ovrlp_start])
	
	ovrlp_sdf = ovrlp_sdf.withColumn('proportion_of_query', ovrlp_sdf[ovrlp_len] / ovrlp_sdf[q_len])\
						.withColumn('proportion_of_ft', ovrlp_sdf[ovrlp_len] / ovrlp_sdf[ft_len])
	return ovrlp_sdf




##################################################################################################
## Step 3: add annotation specific info - from file or DF
##################################################################################################


#### 3) join overlap sDF with annotation specific file
## helper fxns
def add_annot_info_from_df(ovrlp_sdf, annot_info_sdf, join_col_ovrlp, join_col_annot):
	print("running add_annot_info_from_df fxn")
	
	## expression for join operation
	if join_col_ovrlp == join_col_annot:
		on_expr = join_col_ovrlp
	else:
		on_expr = (F.col(join_col_ovrlp)==F.col(join_col_annot))
	
	## join overlap & annot info DFs
	join_sdf = ovrlp_sdf.join(annot_info_sdf, on=on_expr, how='left')
	
	## reorder output columns
	reorder_cols = ovrlp_sdf.columns + annot_info_sdf.columns
	reorder_cols.remove(join_col_ovrlp)
	reorder_cols.remove(join_col_annot)
	
	return join_sdf.select(reorder_cols + [join_col_ovrlp])



def load_annot_info_file(annot_info_file, sep, spark_session):
	print("running load_annot_info_file fxn")
	
	if not os.path.isfile(annot_info_file): ## temporary
		print("ERROR: the specified tissue info file path is NOT valid")
		return None ## change this?
	
	## load annot info file
	annot_info_sdf = sdf_fxn.load_with_header(annot_info_file, sep, spark_session)
	return annot_info_sdf



def add_annot_info_from_file(ovrlp_sdf, annot_info_file, join_col_ovrlp, join_col_annot, spark_session, sep=','):
	print("running add_annot_info_from_file fxn")
	
	## load tissue info file
	annot_info_sdf = load_annot_info_file(annot_info_file, sep, spark_session)
	
	## join overlap sDF & tissue info sDF
	ovrlp_annot_info_sdf = add_annot_info_from_df(ovrlp_sdf, annot_info_sdf, join_col_ovrlp, join_col_annot)
	
	return ovrlp_annot_info_sdf



##################################################################################################
## Step 4: interval summary
##################################################################################################

def interval_summary(ovrlp_sdf, annot_type, annot_abr, query_cols, ft_cols, ovrlp_cols):
	print("running interval_summary fxn")
	
	ft_name_col = ft_cols[0].split('_')[0] + '_name'
	
	## drop duplicates ** NOTE: some may still exist if hit string / 5th col in bed6 is included
	ovrlp_grp =  ovrlp_sdf.dropDuplicates().groupBy(query_cols)
	
	
	#TODO: decide what to do with HOMER duplicates!!! if count('*') -> homer DF will contain duplicates!!
	summary_sdf = ovrlp_grp.agg(F.count('*').alias('num_'+annot_abr+'_ovrlps'),
								  F.countDistinct(*ovrlp_cols).alias('num_'+annot_abr+'_ovrlp_coord'),
								  F.countDistinct(*ft_cols).alias('num_'+annot_abr+'_ft_coord'),
								  F.countDistinct(ft_name_col).alias('num_'+annot_abr+'_ft_names'),
								  F.collect_set(ft_name_col).alias(annot_abr+'_ft_names'))
	# summary_sdf.show(3)
	return summary_sdf



def interval_summary_tissue(ovrlp_sdf, annot_type, annot_abr, query_cols, ft_cols, ovrlp_cols):
	print("running interval_summary_tissue fxn")
	
	ovrlp_grp =  ovrlp_sdf.dropDuplicates().groupBy(query_cols)

	summary_sdf = ovrlp_grp.agg(F.count('*').alias('num_'+annot_abr+'_ovrlps'),
								  F.countDistinct(*ovrlp_cols).alias('num_'+annot_abr+'_ovrlp_coord'),
								  F.countDistinct(*ft_cols).alias('num_'+annot_abr+'_ft_coord'),
								  F.countDistinct('tissue_name').alias('num_'+annot_abr+'_tissues'),
								  F.collect_set('tissue_name').alias(annot_abr+'_tissues'),
								  F.countDistinct('tiss_cat_name').alias('num_'+annot_abr+'_classes'),
								  F.collect_set('tiss_cat_name').alias(annot_abr+'_classes'))
	# summary_sdf.show(3)

	if ('roadmap' in annot_type.lower()) | ('chromHMM' in annot_type.lower()):
		ft_name_col = ft_cols[0].split('_')[0] + '_name'
		hmm_state_tmp_sdf = ovrlp_grp.agg(F.countDistinct(ft_name_col).alias('num_hmm_enh_states'),
										  F.collect_set(ft_name_col).alias('hmm_enh_states'))

		summary_sdf = summary_sdf.join(hmm_state_tmp_sdf, on=query_cols, how='outer')

	return summary_sdf




##################################################################################################
## Step 5: tissue specific
##################################################################################################

def get_tissue_cat_total_counts(tiss_info_sdf, annot_type, annot_abr, ft_cols, ovrlp_cols, num_tissue_cat, spark_session):
	print("running get_tissue_cat_total_counts fxn")
	
	## create tmp sDF with all Tissue Classes & specify the Annotation & Feature type:
	tmp_pd_df = pd.DataFrame([x for x in range(1, num_tissue_cat+1)], columns=['tiss_cat_id'])
	tmp_tclass_sdf = spark_session.createDataFrame(tmp_pd_df)
	# tmp_tclass_sdf.show(3)
	
	## get Tissue Class totals:
	tiss_cat_totals_sdf = tiss_info_sdf.dropDuplicates()\
									.groupBy('tiss_cat_id')\
									.agg(F.count('*').alias('num_' + annot_abr + '_ovrlp'),
										 F.countDistinct('tissue_name').alias('num_' + annot_abr + '_tissue_w_ovrlp'),
										 F.countDistinct(*ovrlp_cols).alias('num_' + annot_abr + '_ovrlp_coord'),
										 F.countDistinct(*ft_cols).alias('num_' + annot_abr + '_ft_coord'))
	# tiss_cat_totals_sdf.show(3)

	## join Tissue Class totals with full list of Tissue Classes (in case 1+ TC doesn't have any overlaps in a data set)
	tc_totals_sdf = tiss_cat_totals_sdf.join(tmp_tclass_sdf, on='tiss_cat_id', how='outer')\
								.na.fill(0)\
								.orderBy('tiss_cat_id')

	return tc_totals_sdf


def generate_tiss_category_matrix(tiss_info_sdf, query_cols, grpby_cols, col_tissue, col_category, col_cat_id, num_tissue_classes):
	print("running generate_tiss_category_matrix fxn")
	
	tc_cols_rename_dict = dict(zip([str(x) for x in range(1, num_tissue_classes+1)],
								   ['tc'+str(y) for y in range(1, num_tissue_classes+1)]))

	## create groupBy object:
	grp = tiss_info_sdf.select(grpby_cols + [col_tissue, col_category, col_cat_id])\
									.dropDuplicates()\
									.orderBy(grpby_cols + [col_tissue])\
									.groupBy(grpby_cols)

	## collect list of tissues & tissue categories
	tiss_list_sdf = grp.agg(F.collect_set(col_tissue).alias('tissues'))
	# tiss_list_sdf.show(3)
	
	tiss_cat_list_sdf = grp.agg(F.collect_set(col_category).alias('tiss_classes'))
	# tiss_cat_list_sdf.show(3)

	## create Tissue Class Matrix columns (values = # of distinct Tissues per TC)
	tc_num_tiss_sdf = grp.pivot(col_cat_id, [x for x in range(1, num_tissue_classes+1)])\
							.agg(F.collect_list(col_tissue))
	## rename columns
	tc_num_tiss_sdf = sdf_fxn.rename_spark_columns(tc_num_tiss_sdf, tc_cols_rename_dict)
	
	new_TCM_sdf = tiss_list_sdf.join(tiss_cat_list_sdf, on=grpby_cols, how='outer')\
							.join(tc_num_tiss_sdf, on=grpby_cols, how='outer')\
							.orderBy(query_cols)
	
	return new_TCM_sdf




##################################################################################################
## Post Annotation processing join individual annot DFs
##################################################################################################

def join_interval_summary(query_sdf, summary_sdf_list, query_cols):
	print("running join_interval_summary fxn")
	
	## outer join DataFrames
	join_sdf = query_sdf
	for summ_sdf in summary_sdf_list:
		join_sdf = join_sdf.join(summ_sdf, on=query_cols, how='outer')
	
	## fill in missing values
	fill_0_cols = [c for c in join_sdf.columns if 'num_' in c]
	join_sdf = join_sdf.fillna(0, subset=fill_0_cols)
	join_sdf = join_sdf.fillna("None")
	
	return join_sdf


def join_tissue_category_counts(tc_cnt_sdf_list, col_tiss_cat):
	print("running join_tissue_category_counts fxn")
	
	join_sdf = tc_cnt_sdf_list[0]
	for tc_cnt in tc_cnt_sdf_list[1:]:
		join_sdf = join_sdf.join(tc_cnt, on=col_tiss_cat, how='outer')
	
	join_sdf = join_sdf.orderBy(col_tiss_cat)
	
	return join_sdf




######################## Multiway overlap by Tissue category

def two_way_overlap(sdf1, sdf2, cols_join, start1, start2, end1, end2, col_prefix):
	print("running two_way_overlap fxn")
	
	## set up variables
	twoway_start = col_prefix + '_start'
	twoway_end = col_prefix + '_end'
	twoway_len = col_prefix + '_len'
	
	## join overlap DFs
	# join_expr = ((F.least(F.col(end1), F.col(end2))+1 - F.greatest(F.col(start1), F.col(start2))) >=1 )
	join_sdf = sdf1.join(sdf2, on=cols_join, how="inner")
	
	## calc two way overlap coord & length
	join_sdf = join_sdf.withColumn(twoway_start, F.greatest(F.col(start1), F.col(start2)))\
						.withColumn(twoway_end, F.least(F.col(end1), F.col(end2)))
	join_sdf = join_sdf.withColumn(twoway_len, F.col(twoway_end)+1 - F.col(twoway_start))
	# print("count before filter:\t", join_sdf.count())
	
	## use twoway overlap length to filter out non-overlapping pairs
	join_filt_sdf = join_sdf.filter(join_sdf[twoway_len] >= 1)
	# print("count after filter:\t", join_filt_sdf.count())
	return join_filt_sdf


def fantom5_roadmap_overlap(f5_tcm_sdf, rm_tcm_sdf, q_cols, ovrlp_cols):
	#### variable setup
	o_str = ovrlp_cols[0].split('_')[0]
	join_cols = q_cols
	if o_str+'_chr' in f5_tcm_sdf.columns:
		join_cols = join_cols + [o_str+'_chr']
	
	start_end_cols = [o_str+'_start_f5', o_str+'_start_rm', o_str+'_end_f5', o_str+'_end_rm']
	twoway_col_prefix = "f5_rm_ovrlp"
	
	## rename FANTOM5 & Roadmap columns
	rename_f5_dict = {col: col+'_f5' for col in f5_tcm_sdf.columns if col not in join_cols}
	rename_rm_dict = {col: col+'_rm' for col in rm_tcm_sdf.columns if col not in join_cols}
	# print(rename_f5_dict)
	
	f5_tcm_renamed_sdf = sdf_fxn.rename_spark_columns(f5_tcm_sdf, rename_f5_dict)
	rm_tcm_renamed_sdf = sdf_fxn.rename_spark_columns(rm_tcm_sdf, rename_rm_dict)
	
	## two way overlap: FANTOM5 vs Roadmap
	f5_rm_tcm_sdf = two_way_overlap(f5_tcm_renamed_sdf, rm_tcm_renamed_sdf, join_cols, *(start_end_cols + [twoway_col_prefix]))
	
	##TODO: reorder columns
	
	return f5_rm_tcm_sdf



def count_twoway_ovrlp_by_tiss_cat(twoway_sdf, annot_abr1, annot_abr2, col_cat_id, num_tiss_cat, spark_session):
	## setup
	tc_cols = ['tc'+str(x) for x in range(1, num_tiss_cat+1)]
	expr_tc_cnt = [ F.count(F.when((F.col(c+'_'+annot_abr1) != '') & (F.col(c+'_'+annot_abr2) != '') , True)).alias(c.strip('tc')) for (c) in tc_cols ]
	col_cnt = 'num_' + annot_abr1 + '+' + annot_abr2 + '_ovrlps'
	
	## count # of twoway overlaps per Tissue Category
	tc_cnt_sdf = twoway_sdf.agg(*expr_tc_cnt)
	
	## transpose PySpark DF via Pandas
	tc_cnt_sdf_T = tc_cnt_sdf.toPandas().T
	tc_cnt_sdf_T.columns = [col_cnt]
	tc_cnt_sdf_T[col_cat_id] = tc_cnt_sdf_T.index
	tc_cnt_sdf_T = tc_cnt_sdf_T[[col_cat_id, col_cnt]]
	
	## convert back to PySpark DF
	tc_cntT_sdf = spark_session.createDataFrame(tc_cnt_sdf_T)
	
	return tc_cntT_sdf





##################################################################################################
## write output files
##################################################################################################

def write_interval_summary_output_file(summ_sdf, summ_sdf_name, query_cols, out_dir, shell_dir):
	print("running write_interval_summary_output_file fxn")
	
	## 1) specify output file name:
	out_file_name = summ_sdf_name + '_interval_summary.tsv'
	
	## 2) check if summ_sdf contains ArrayType columns --> convert to StringType
	summ_sdf = sdf_fxn.convert_ArrayType_columns(summ_sdf)
	print(summ_sdf.printSchema())
	
	## 3) specify sort cols
	sort_cols = query_cols.copy()
	chr_col = query_cols[0].split('_')[0] + '_chr'
	sort_cols.remove(chr_col)
	
	## 4) call function in spark_df_functions.py module --> write full annotation sdf output file to disk:
	write_sdf_return_code = sdf_fxn.write_sdf_to_text_sort_by_chr(summ_sdf, chr_col, sort_cols, out_dir, out_file_name, '\t', header=True, shell_path=shell_dir)
	
	return write_sdf_return_code


