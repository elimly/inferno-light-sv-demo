#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
*** Spark DataFrame Functions ***

NOTES:
	- these functions have not been time tested
	- some of the functions call shell scripts (located in the src directory in this repo)
"""

import subprocess, os
from itertools import chain
import pyspark.sql.functions as F
from pyspark.sql import Row
# from pyspark.sql import SparkSession
from pyspark.sql.types import *

# TODO: add error checks, returncode, stdout & stderr to log files

#---------------------------------------------------------------#
#### imports that might be necessary for future functions:
# from pyspark.sql import SparkSession
# from pyspark.sql.types import *
#---------------------------------------------------------------#

def print_sdf(sdf, nrow=5):
	print(sdf.limit(nrow).toPandas().head(nrow))





###############################################################################
#### Functions for renaming Spark DF columns
###############################################################################
#<editor-fold desc="Rename DF columns fxns">

def rename_spark_columns(sdf, col_dict):
	"""rename Spark DF columns using the supplied dictionary

	:param sdf: Spark DataFrame to rename
	:param col_dict: dict of string with format: {'old_name':'new_name'}
			** raises ValueError if col_dict is not properly formatted
	:return: renamed Spark DataFrame | None if col_dict ValueError
	"""

	if isinstance(col_dict, dict):
		for old_name, new_name in col_dict.items():
			sdf = sdf.withColumnRenamed(old_name, new_name)
		return sdf
	else:
		raise ValueError("'col_dict' should be a dict with format:  {'old_name_1':'new_name_1', 'old_name_2':'new_name_2'}")
		# return None


def rename_spark_columns_lowercase(sdf):
	"""convert Spark DF column names to lowercase

	:param sdf: Spark DataFrame with columns to rename
	:return: renamed Spark DataFrame
	"""

	columns = dict(zip(sdf.columns, [c.lower() for c in sdf.columns]))
	for old_name, new_name in columns.items():
		sdf = sdf.withColumnRenamed(old_name, new_name)
	return sdf

#</editor-fold>





###############################################################################
#### Helper Function to convert ArrayType column to concatenated StringType
###############################################################################

def convert_ArrayType_columns(sdf):
	"""
	Checks if the DataFrame contains any ArrayType columns & converts them to StringType
	* necessary for write.csv *
	
	:param sdf: Spark DF that may or may not contain ArrayType columns
	:return: Spark DF with NO ArrayType columns
	"""
	## file cols of ArrayType
	array_cols = [sf.name for sf in sdf.schema.fields if "ArrayType" in str(sf.dataType)]
	
	## convert ArrayType columns to StringType
	if len(array_cols) > 0:
		for ary_col in array_cols:
			sdf = sdf.withColumn(ary_col, F.concat_ws(', ', sdf[ary_col]))
	
	return sdf





###############################################################################
#### Helper Function to convert chr string column to integer
###############################################################################
#<editor-fold desc="Helper Fxns for converting 'chr*' string to int">

def generate_chr_mapping():
	"""
	Creates a PySpark mapping object chrom str -> chrom int
	Used for adding new DF column with chromosome as int => sorting
	
	:return: PySpark mapping object chrom str -> chrom int
	"""
	## generate dict for casting chrs to int:
	chr_dict = {'chr'+str(x):x for x in range(1, 23)}
	chr_dict['chrX'] = 23
	chr_dict['chrY'] = 24
	chr_dict['chrMT'] = 25
	
	## generate Pyspark mapping object:
	chr_mapping = F.create_map([F.lit(x) for x in chain(*chr_dict.items())])
	return chr_mapping


def add_chr_int_column(sdf, old_chr_col, new_col_name, chr_mapping):
	"""
	Helper fxn that adds a new IntegerType column with chromosome as int
	
	:param sdf: Spark DF
	:param old_chr_col: (str) name of the StringType chromosome column
	:param new_col_name: (str) name of the new IntegerType chromosome column
	:param chr_mapping: PySpark mapping object chrom str -> int
	:return: Spark DF with new IntegerType chromosome column
	"""
	sdf = sdf.withColumn(new_col_name, chr_mapping.getItem(F.col(old_chr_col)))
	return sdf

#</editor-fold>


###############################################################################
#### Sort DF by chr column
###############################################################################
def sort_sdf_by_chr(sdf, chr_col, sort_cols_noChr):
	"""
	Sort Spark DataFrame by chromosome column
	
	Spark sorts numbers in str as str so the chromosomes will be in the wrong order.
	=> convert to int 1st => sort int col
	
	:param sdf: Spark DF to sort
	:param chr_col: (str) name of chromosome column to sort
	:param sort_cols_noChr: (list) other non-chromosome containing columns to sort
	:return: sorted Spark DF
	"""
	## create chr mapping object -> add new column with chromosome as int
	chr_int_mapping = generate_chr_mapping()
	sdf = add_chr_int_column(sdf, chr_col, "chrN", chr_int_mapping)
	
	cols_output = sdf.columns
	cols_output.remove("chrN")
	
	## sort DF and repartition
	sdf = sdf.orderBy(["chrN"] + sort_cols_noChr)\
			.select(cols_output)
	return sdf





###############################################################################
#### Functions for transposing Spark DFs / Spark DF Rows
###############################################################################

def transpose_spark_df_row(row, spark_context, row_label="value", cast_type=None):
	"""
	Transpose single Row to DF column (using Spark Row.asDict())
	
	:param row: (Row) a Spark DF Row object to transpose
	:param spark_context: current Spark Context
	:param row_label: (str) new Column name for the transposed Row
	:param cast_type: (str) type the new column will be casted to
	:return: Spark DF
	"""
	row_list = [Row(key=k, value=str(v)) for k,v in row.asDict().items()]
	sdf = spark_context.parallelize(row_list).toDF(["key", row_label])
	
	if cast_type:
		sdf = sdf.withColumn(row_label, sdf[row_label].cast(cast_type))
	return sdf


def transpose_spark_df(df, spark_context, cast_type=None):
	"""
	Transpose a Spark DataFrame
	* NOTE for smallish DFs: using .toPandas().T is much FASTER!!
	
	:param df: a Spark DF to transpose
	:param spark_context: current Spark Context
	:param cast_type: (str) type the new column will be casted to
	:return: transposed Spark DF
	"""
	
	# variables
	df_T_num_cols = df.count()
	df_T_cols = ['column_name'] + ['_row'+str(i) for i in range(0, df_T_num_cols)]
	
	dataType_dict = {'string':StringType(), 'int':IntegerType(), 'double':DoubleType(), 'float':FloatType(), 'long':LongType()}
	dataType = StringType() ## default: cast to string, unless otherwise specified
	
	## generate aggregate expressions:
	if cast_type is None: ## do not cast
		agg_expr = [F.collect_list(c).alias(c) for c in df.columns]
	else:
		if cast_type in dataType_dict: ## cast to specified Spark DataType
			dataType = dataType_dict[cast_type]
		agg_expr = [F.collect_list(c).cast(ArrayType(dataType)).alias(c) for c in df.columns]
	
	## 1) aggregate each column --> list
	df_list_sdf = df.groupBy().agg(*agg_expr)
	
	## 2) extract the 1st row values
	df_row = df_list_sdf.collect()[0]

	## 3) transpose sDF (create new sDF with row: col name + list of col values in original sDF)
	df_T_sdf = spark_context.parallelize([[c] + df_row[c] for c in df_list_sdf.columns])\
							.toDF(df_T_cols)
	return df_T_sdf


def sdf_to_long(df):
	"""
	Helper fxn to reshape Spark DF to long format
	
	:param df: Spark DataFrame
	:return: Spark DataFrame in long format
	"""
	key_value_structs = F.explode(
		F.array([F.struct(F.lit(c).alias("key"), F.col(c).cast("string").alias("val")) for c in df.columns])).alias("kvs")
	return df.select(key_value_structs).select(["kvs.key", "kvs.val"])






###############################################################################
#### Functions for loading text (txt|csv|tsv|bed) files to Spark DataFrames
###############################################################################
#<editor-fold desc="Load file -> DF fxns">
def load_with_header(file_path, delim, spark_session):
	"""Load text file to DataFrame and specify the column names
	
	***NOTE: this assumes the column name list is correctly ordered
	
	:param file_path: (str) path to file
	:param delim: (str) the file's delimiter / separator
	:param spark_session: current Spark session
	:return: Spark DataFrame
	"""
	if os.path.isfile(file_path):
		try:
			## read file to DataFrame
			sdf = spark_session.read.csv(file_path, sep=delim,
			                             header=True, inferSchema='true')
			return sdf
		
		except IOError:
			print("FileIO ERROR: cannot load the specified file")
	else:
		print("FileIO ERROR: the specified file / path does NOT exist")
		return None



def load_name_columns(file_path, delim, col_names, spark_session):
	"""Load text file to DataFrame and specify the column names
	
	***NOTE: this assumes the column name list is correctly ordered
	
	:param file_path: (str) path to file
	:param delim: (str) the file's delimiter / separator
	:param col_names: (list) column names in their correct order
	:param spark_session: current Spark session
	:return: Spark DataFrame
	"""
	if os.path.isfile(file_path):
		try:
			## check if input file contains header row:
			with open(file_path) as f:
				line0 = f.readline().lower().strip()
			f.close()
			
			if line0 == delim.join(col_names).lower():
				has_header = True
			else:
				has_header = False
			
			## read file to DataFrame
			sdf = spark_session.read.csv(file_path, sep=delim,
			                             header=has_header, inferSchema='true')
			
			## rename each column:
			for i in range(len(col_names)):
				sdf = sdf.withColumnRenamed('_c'+str(i), col_names[i])
			return sdf
		
		except IOError:
			print("FileIO ERROR: cannot load the specified file")
	else:
		print("FileIO ERROR: the specified file / path does NOT exist")
		return None
	


def load_specify_schema(file_path, input_schema, delim, spark_session):
	"""function to load input SV bed file into Spark DF
	
	:param file_path: (str) path to file
	:param input_schema: Spark StructType object
	:param delim: (str) the file's delimiter / separator
	:param spark_session: current Spark session
	:return: Spark DataFrame
	"""
	if os.path.isfile(file_path):
		try:
			sdf = spark_session.read.csv(file_path, schema=input_schema,
			                             sep=delim)
			return sdf
		except IOError:
			print("FileIO ERROR: cannot load the specified file")
	else:
		print("FileIO ERROR: the specified file / path does NOT exist")
		return None



def input_sv_file_to_sdf(input_sv_file, input_schema, spark_session):
	"""SV-INFERNO function to load input SV bed file into Spark DF
	
	:param input_sv_file: (str) path to bed file
	:param input_schema: Spark StructType object
	:param spark_session: current Spark session
	:return: Spark DataFrame
	"""
	if os.path.isfile(input_sv_file):
		try:
			## check if input file contains header row:
			with open(input_sv_file) as f:
				line0 = f.readline().lower()
			f.close()
			
			if ("start" in line0) | ("end" in line0):
				has_header = True
			else:
				has_header = False
			
			## read file --> Spark DataFrame
			input_sv_sdf = spark_session.read.csv(input_sv_file, header=has_header,
			                                      schema=input_schema, sep='\t')
			
			## repartition sdf based on SV coords
			input_sv_sdf = input_sv_sdf.repartition('sv_chr', 'sv_start', 'sv_end')
			return input_sv_sdf
			
		except IOError:
			print("FileIO ERROR: cannot load the specified file")
			return None
		
#</editor-fold>
	



###############################################################################
#### Functions for writing Spark DataFrames to output files
###############################################################################
#<editor-fold desc="Write DF -> file fxns">

def write_sdf_to_text_sort_by_chr(sdf, chr_col, sort_cols_noChr, output_path, file_name, delim, header=False, shell_path='src', file_ext='.txt'):
	"""Generic function to write Spark DF to any type of text file
	with the specified deliminator using the Pyspark write.csv function

	**NOTE: the file name should include the extension!**

	:param sdf: Spark DataFrame to write to file
	:param chr_col: column name containing chromosome str
	:param sort_cols_noChr: (list - str) NON-chromosome columns to sort by
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:param delim: (str) [unicode] string indicating column separator char
	:param header: (bool) default=False
	:param shell_path: path to shell script dir
	:return: (int) 0 == success | 1 == failure
	"""
		## check if file name contains a file extension (.csv | .txt | .tsv)
	ext = ['.csv', '.tsv', '.txt']
	if not any(e in file_name for e in ext):
		file_name = file_name + file_ext
	
	## set up dir paths
	tmp_dir = os.path.join(output_path, 'tmp')
	shell_script = os.path.join(shell_path, 'convert_spark_output.sh')
	
	## create chr mapping object -> add new column with chromosome as int
	chr_int_mapping = generate_chr_mapping()
	sdf = add_chr_int_column(sdf, chr_col, "chrN", chr_int_mapping)
	
	cols_output = sdf.columns
	cols_output.remove("chrN")
	
	## sort DF and repartition
	sdf = sdf.orderBy(["chrN"] + sort_cols_noChr)\
			.select(cols_output)\
			.repartition(1)

	## write Spark DF to csv file in tmp output directory
	sdf.write.csv(tmp_dir, header=header, sep=delim, mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return 1

	## shell script: 1) rename file, 2) remove tmp directory & _SUCCESS file
	response = subprocess.run(
			[shell_script, tmp_dir, output_path, file_name], shell=False)
	
	return_code = response.returncode
	# stdout = response.stdout
	# stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	return return_code



def write_sdf_to_text(sdf, output_path, file_name, delim, header=False, shell_path='src', file_ext='.txt'):
	"""Generic function to write Spark DF to any type of text file
	with the specified deliminator using the Pyspark write.csv function

	**NOTE: the file name should include the extension!**

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:param delim: (str) [unicode] string indicating column separator char
	:param header: (bool) default=False
	:param shell_path: path to shell script dir
	:return: (int) 0 == success | 1 == failure
	"""
	## check if file name contains a file extension (.csv | .txt | .tsv)
	ext = ['.csv', '.tsv', '.txt']
	if not any(e in file_name for e in ext):
		file_name = file_name + file_ext
	
	## set up dir paths
	tmp_dir = os.path.join(output_path, 'tmp')
	shell_script = os.path.join(shell_path, 'convert_spark_output.sh')
	
	## repartition DF
	sdf = sdf.repartition(1)

	## write Spark DF to csv file in tmp output directory
	sdf.write.csv(tmp_dir, header=header, sep=delim, mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return 1

	## shell script: 1) rename file, 2) remove tmp directory & _SUCCESS file
	response = subprocess.run(
			[shell_script, tmp_dir, output_path, file_name], shell=False)
	
	return_code = response.returncode
	# stdout = response.stdout
	# stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	return return_code



def write_sdf_to_text_postsort(sdf, output_path, file_name, delim, header=False, shell_path='src', file_ext='.txt'):
	"""Generic function to write Spark DF to any type of text file
	with the specified deliminator using the Pyspark write.csv function

	**NOTE: the file name should include the extension!**

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:param delim: (str) [unicode] string indicating column separator char
	:param header: (bool) default=False
	:param shell_path: path to shell script dir
	:return: (int) 0 == success | 1 == failure
	"""
	## check if file name contains a file extension (.csv | .txt | .tsv)
	ext = ['.csv', '.tsv', '.txt']
	if not any(e in file_name for e in ext):
		file_name = file_name + file_ext
	
	## set up dir paths
	tmp_dir = os.path.join(output_path, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write.csv(tmp_dir, header=header, sep=delim, mode="overwrite")

	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code

	## shell script: 1) sort file, 2) move file, 3) remove tmp directory
	if header:
		shell_script = os.path.join(shell_path, 'sort_spark_output_with_header.sh')
		response = subprocess.run(
			[shell_script, tmp_dir, output_path, file_name], shell=False)

	else:
		shell_script = os.path.join(shell_path, 'sort_spark_output_no_header.sh')
		response = subprocess.run(
			[shell_script, tmp_dir, output_path, file_name], shell=False)
	
	return_code = response.returncode
	# stdout = response.stdout
	# stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	return return_code



def write_sorted_sdf_to_text(sdf, sort_cols, output_path, file_name, delim, header=False, shell_path='src', file_ext='.txt'):
	"""Generic function to write Spark DF to any type of text file
	with the specified deliminator using the Pyspark write.csv function

	**NOTE: the file name should include the extension!**

	:param sdf: Spark DataFrame to write to file
	:param sort_cols: (list - str) column to sort by
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:param delim: (str) [unicode] string indicating column separator char
	:param header: (bool) default=False
	:param shell_path: path to shell script dir
	:return: (int) 0 == success | 1 == failure
	"""
	## check if file name contains a file extension (.csv | .txt | .tsv)
	ext = ['.csv', '.tsv', '.txt']
	if not any(e in file_name for e in ext):
		file_name = file_name + file_ext
	
	## set up dir paths
	tmp_dir = os.path.join(output_path, 'tmp')
	shell_script = os.path.join(shell_path, 'convert_spark_output.sh')
	
	## sort DF and repartition
	sdf = sdf.orderBy(sort_cols)\
			.repartition(1)

	## write Spark DF to csv file in tmp output directory
	sdf.write.csv(tmp_dir, header=header, sep=delim, mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return 1

	## shell script: 1) rename file, 2) remove tmp directory & _SUCCESS file
	response = subprocess.run(
			[shell_script, tmp_dir, output_path, file_name], shell=False)
	
	return_code = response.returncode
	# stdout = response.stdout
	# stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	return return_code



#------------------------------------------------------------------------------#
## write to bed file
def write_sdf_to_bed_file(sdf, output_path, file_name, shell_path='src'):
	"""write Spark DF to bed file

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param file_name: (str) name of file  **include the extension!**
	:param shell_path: path to shell script dir
	:return: (int) 0 == success | 1 == failure
	"""
	tmp_dir = os.path.join(output_path, 'tmp')
	shell_script = os.path.join(shell_path, 'sort_spark_output_to_bed.sh')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write.csv(tmp_dir, header="false", sep='\t', mode="overwrite")
	
	## test if write.csv() worked:
	if not os.path.isfile(os.path.join(tmp_dir, '_SUCCESS')):
		return_code = 1
		print("return code = ", return_code)
		print("ERROR: Spark write.csv() failed")
		return return_code
	
	## shell script: 1) sort csv -> .bed file, 2) remove tmp directory
	response = subprocess.run(
		[shell_script, tmp_dir, output_path, file_name], shell=False)
	
	return_code = response.returncode
	# stdout = response.stdout
	# stderr = response.stderr
	
	print("return code = ", return_code)
	if return_code == 0:
		print("Spark DF successfully saved to .bed file")
	else:
		print("ERROR: convert Spark subprocess failed (but Spark write.csv() success)")
	return return_code

#</editor-fold>





###############################################################################
#### Functions for writing & reading sdf <--> parquet file
###############################################################################
#<editor-fold desc="PARQUET file load / write fxns">

def load_sdf_from_parquet(parquet_dir, spark_session):
	"""generalized function that loads Spark DF from parquet files

	:param parquet_dir: (str) name for parquet directory
	:param spark_session: (str) current Spark session name
	:return: Spark DataFrame
	"""
	
	if os.path.isdir(parquet_dir):
		try:
			sdf = spark_session.read.parquet(parquet_dir)
			return sdf
		except IOError:
			print("FileIO ERROR: cannot load the specified directory")
	else:
		print("FileIO ERROR: the specified directory does NOT exist")
		return None



def write_sdf_to_parquet(sdf, output_path, parquet_dir_name, compression="gzip", partition_cols=None):
	"""Generalized function that writes Spark DF to parquet file

		*NOTE*: using the partitionBy option is EXTREMELY SLOW!

	:param sdf: Spark DataFrame to write to file
	:param output_path: (str) path for output file
	:param parquet_dir_name: (str) name of the parquet directory
	:param compression: (str) indicating compression type (default = gzip)
	:param partition_cols: (list) columns to partition on
	:return: (int) 0 == success | 1 == failure
	"""
	parquet_name = os.path.join(output_path, parquet_dir_name)

	if partition_cols is None:
		sdf.write\
			.parquet(parquet_name, mode='overwrite', compression=compression)
	else:
		sdf.write\
			.partitionBy(partition_cols)\
			.parquet(parquet_name, mode='overwrite', compression=compression)

	## test if write was successful
	if os.path.isfile(os.path.join(parquet_name, '_SUCCESS')):
		return_code = 0
		print("return code = ", return_code)
		print("Spark DF successfully saved to parquet")
	else:
		return_code = 1
		print("ERROR occurred when trying to save Spark DF to parquet")
	
	return return_code


#</editor-fold>




